﻿
using CustomerFeedback.Domain.DataAccessLayer;
using CustomerFeedback.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using RestSharp.Serializers;
using System.Web.Script.Serialization;


namespace CustomerFeedback.Domain.BusinessLogicLayer
{
    public class Wallet_TransactionDetailBusinessLogic
    {
        private string UsedDB;

        #region Constructor

        public Wallet_TransactionDetailBusinessLogic()
        {
            UsedDB = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
        }

        #endregion
        
        public bool Wallet_CreateFeedBack(FeedBackModel reqModel)
        {
            var dataLayer = new Wallet_OTCDataAccess();

            if(int.Parse(reqModel.rating) <3)
            {
                foreach (var feedbacktypeId in reqModel.lstFreeBackTypeID)
                {
                    dataLayer.Wallet_CreateCustomerFeedBack(reqModel, feedbacktypeId);
                }
            }
            else
            {
                dataLayer.Wallet_CreateCustomerFeedBack(reqModel, reqModel.feedbacktypeid);
            }

            return true;
        }
    }
}