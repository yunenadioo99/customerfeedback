﻿using CustomerFeedback.Domain.Models;
using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;

namespace CustomerFeedback.Domain.DataAccessLayer
{
    public class Wallet_OTCDataAccess
    {
        #region Declaration

        private string strConnection;
        MySqlTransaction transaction;
        public bool useTransaction = false;
        MySqlConnection Connection = new MySqlConnection();

        #endregion
        
        #region Properties

        MySqlTransaction Transaction
        {
            get { return this.transaction; }

            set { this.transaction = value; }
        }

        #endregion

        #region Constructor

        public Wallet_OTCDataAccess()
        {
            strConnection = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
            Connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);
        }

        #endregion

        #region Transaction Methods

        public void StartTransaction()
        {
            if (this.Connection.State == ConnectionState.Closed)
            {
                this.Connection.Open();
            }

            this.Transaction = this.Connection.BeginTransaction();
            this.useTransaction = true;
        }

        public void CommitTransaction()
        {
            if ((this.Transaction != null) && (this.Connection.State == ConnectionState.Open))
            {
                this.Transaction.Commit();

                if (this.Connection.State == ConnectionState.Open)
                {
                    this.Connection.Close();
                }

                this.useTransaction = false;
            }
        }

        public void RollbackTransaction()
        {
            if ((this.Transaction != null) && (this.Connection.State == ConnectionState.Open))
            {
                this.Transaction.Rollback();

                if (this.Connection.State == ConnectionState.Open)
                {
                    this.Connection.Close();
                }

                this.useTransaction = false;
            }
        }

        #endregion

        /// <summary>
        /// Get an object's value or DBNull value
        /// </summary>
        /// 
        /// <param name="obj">The Object</param>
        /// <returns>Object's value or DBNull value</returns>
        protected virtual object GetNull(object obj)
        {
            // Check that object is "string" and object's value is empty string
            if ((obj is string) && (obj.ToString() == string.Empty))
            {
                // Specify DBNull value
                obj = DBNull.Value;
                return obj;
            }

            // Check that object is "DateTime" and object's value is minimum datetime value
            if ((obj is DateTime) && (((DateTime)obj) == DateTime.MinValue))
            {
                // Specify DBNull value
                obj = DBNull.Value;
                return obj;
            }

            // Check that object is "int" and object's value is minimum integer value
            if ((obj is int) && (((int)obj) == -2147483648))
            {
                // Specify DBNull value
                obj = DBNull.Value;
                return obj;
            }

            // Check that object is "float" and object's value is minimum float value
            if ((obj is float) && (((float)obj) == float.MinValue))
            {
                // Specify DBNull value
                obj = DBNull.Value;
                return obj;
            }

            // Check that object is "decimal" and object's value is minimum decimal value
            if ((obj is decimal) && (((decimal)obj) == -79228162514264337593543950335M))
            {
                // Specify DBNull value
                obj = DBNull.Value;
                return obj;
            }

            // Check that object is "double" and object's value is minimum double value
            if ((obj is double) && (((double)obj) == double.MinValue))
            {
                // Specify DBNull value
                obj = DBNull.Value;
            }

            return obj;
        }

        #region Customer FeedBack
        public bool Wallet_CreateCustomerFeedBack(FeedBackModel reqModel, string feedbacktypeid)
        {
            MySqlCommand cmd = null;
            try
            {
                using (MySqlConnection conn = new MySqlConnection(strConnection))
                {
                    conn.Open();
                    cmd = new MySqlCommand();
                    cmd = conn.CreateCommand();
                    cmd.CommandText =
                        "INSERT INTO customerfeedback (branchid,rating,type,feedbacktypeid,phone," +
                        "email,createddatetime,describe,provide) " +
                        "VALUES (@branchid,@rating,@type,@feedbacktypeid,@phone," +
                        "@email,@createddatetime,@describe,@provide);";
                    cmd.Parameters.AddWithValue("@branchid", reqModel.branchid);
                    cmd.Parameters.AddWithValue("@rating", reqModel.rating);
                    cmd.Parameters.AddWithValue("@type", reqModel.type);
                    cmd.Parameters.AddWithValue("@feedbacktypeid", feedbacktypeid);
                    cmd.Parameters.AddWithValue("@phone", reqModel.phone);
                    cmd.Parameters.AddWithValue("@email", reqModel.email);
                    cmd.Parameters.AddWithValue("@createddatetime", DateTime.Now);
                    cmd.Parameters.AddWithValue("@describe", reqModel.describe);
                    cmd.Parameters.AddWithValue("@provide", reqModel.provide);


                    var affectedRows = cmd.ExecuteNonQuery();
                    conn.Close();
                    if (affectedRows > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd?.Connection.Close();
            }
        }

        #endregion


    }
}