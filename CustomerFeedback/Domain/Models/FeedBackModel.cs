﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerFeedback.Domain.Models
{
    public class FeedBackModel
    {
        public string branchid { get; set; }
        public string rating { get; set; }
        public string type { get; set; }
        public string feedbacktypeid { get; set; }
        public List<string> lstFreeBackTypeID { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string describe { get; set; }
        public string provide { get; set; }
        
    }
}