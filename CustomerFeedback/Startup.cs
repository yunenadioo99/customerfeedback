﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CustomerFeedback.Startup))]
namespace CustomerFeedback
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
