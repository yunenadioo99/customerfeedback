﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerFeedback.Domain.Models;
using CustomerFeedback.Domain.BusinessLogicLayer;
using CustomerFeedback.Common;

namespace CustomerFeedback.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult createFeedback(FeedBackModel std)
        {
            string message = "";
            string errorMsg = "";
            try
            {
                var businessLogic = new Wallet_TransactionDetailBusinessLogic();

                if (businessLogic.Wallet_CreateFeedBack(std))
                    message = "SUCCESS";
                return Json(new { Message = message, JsonRequestBehavior.AllowGet });
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;


            }
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });

        }

        public ActionResult Thankyou()
        {
            return View();
        }
        public ActionResult ChangeCurrentCulture(int id)
        {
            CultureHelper.CurrentCulture = id;
            Session["CurrentCulture"] = id;
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}