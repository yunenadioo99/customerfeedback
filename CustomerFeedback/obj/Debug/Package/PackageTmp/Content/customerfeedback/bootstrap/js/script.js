function highlightStar(obj) {
    removeHighlight();
    var value = $(obj).val();
    var className = "";
    if (value == 1 || value == 2) {
        className = "highlight_red";
    }
    else if (value == 3) {
        className = "highlight";
    }
    else {
        className = "highlight_green";
    }
    $('.li_star').each(function (index) {

        $(this).addClass(className);


        if (index == $(".li_star").index(obj)) {
            return false;
        }
    });
}

function removeHighlight() {
    $('.li_star').removeClass('selected');
    $('.li_star').removeClass('highlight');
    $('.li_star').removeClass('selected_green');
    $('.li_star').removeClass('highlight_green');
    $('.li_star').removeClass('selected_red');
    $('.li_star').removeClass('highlight_red');
}

function addRating(obj) {
    var value = $(obj).val();
    var className = "";
    if (value == 1 || value == 2) {
        className = "selected_red";
        document.getElementById('test').className = "hide";
        document.getElementById('all').className = "open";
        Unselect();
    }
    else if (value == 3) {
        className = "selected";
        document.getElementById('all').className = "hide";
        document.getElementById('test').className = "open";
        Unselect2();
    }
    else {
        className = "selected_green";
        document.getElementById('all').className = "hide";
        document.getElementById('test').className = "open";
        Unselect2();
    }

    $('.li_star').each(function (index) {
        $(this).addClass(className);
        $('#rating').val((index + 1));
        if (index == $(".li_star").index(obj)) {
            return false;
        }
    });
}

function resetRating() {
    var value = $("#rating").val();
    if (value == 1 || value == 2) {
        $('.li_star').each(function (index) {
            $(this).addClass('selected_red');
            if ((index + 1) == $("#rating").val()) {
                return false;
            }
        });
    }
    else if (value == 3) {
        $('.li_star').each(function (index) {
            $(this).addClass('selected');
            if ((index + 1) == $("#rating").val()) {
                return false;
            }
        });
    }
    else if (value == 4 || value == 5) {
        $('.li_star').each(function (index) {
            $(this).addClass('selected_green');
            if ((index + 1) == $("#rating").val()) {
                return false;
            }
        });
    }

}

function highlightStar2(obj) {
    removeHighlight2();
    var value = $(obj).val();
    var className = "";
    if (value == 1 || value == 2) {
        className = "highlight_red";
    }
    else if (value == 3) {
        className = "highlight";
    }
    else {
        className = "highlight_green";
    }
    $('.li_star2').each(function (index) {

        $(this).addClass(className);


        if (index == $(".li_star2").index(obj)) {
            return false;
        }
    });
}

function removeHighlight2() {
    $('.li_star2').removeClass('selected');
    $('.li_star2').removeClass('highlight');
    $('.li_star2').removeClass('selected_green');
    $('.li_star2').removeClass('highlight_green');
    $('.li_star2').removeClass('selected_red');
    $('.li_star2').removeClass('highlight_red');
}

function addRating2(obj) {
    var value = $(obj).val();
    var className = "";
    if (value == 1 || value == 2) {
        className = "selected_red";


    }
    else if (value == 3) {
        className = "selected";


    }
    else {
        className = "selected_green";


    }

    $('.li_star2').each(function (index) {
        $(this).addClass(className);
        $('#rating2').val((index + 1));
        if (index == $(".li_star2").index(obj)) {
            return false;
        }
    });
}

function resetRating2() {
    var value = $("#rating2").val();
    if (value == 1 || value == 2) {
        $('.li_star2').each(function (index) {
            $(this).addClass('selected_red');
            if ((index + 1) == $("#rating2").val()) {
                return false;
            }
        });
    }
    else if (value == 3) {
        $('.li_star2').each(function (index) {
            $(this).addClass('selected');
            if ((index + 1) == $("#rating2").val()) {
                return false;
            }
        });
    }
    else if (value == 4 || value == 5) {
        $('.li_star2').each(function (index) {
            $(this).addClass('selected_green');
            if ((index + 1) == $("#rating2").val()) {
                return false;
            }
        });
    }

}

function validateRadio() {
    var x = $("#rating").val();
    if (x == "") {
        alert("Please choose your rating.");
        return false;
    }
    else {
        if (saveCustomerFeedBack()) {
            return true;
        }


    }



}

function saveCustomerFeedBack() {

    var std = {};
    std.branchid = getUrlParameter('branchid');
    std.rating = $("#rating").val();
    var feedtypeArr = [];

    if (std.rating > 2) {
        std.type = '1';
        std.provideFeedback = $("#messageBox1").val();
        std.phone = $('#phoneno1').val();
        std.email = $('#emailname1').val();
        std.feedbacktypeid = $("#rating2").val();
    }
    else {
        std.type = '0';
        var ele2 = $(".check");
        for (var i = 0; i < ele2.length; i++) {
            if ((ele2[i].checked)) {
                feedtypeArr.push(ele2[i].value);
            }
        }
        std.lstFreeBackTypeID = feedtypeArr;
        std.describeExperience = $('#messageBox2').val();
        std.provideFeedback = $('#messageBox3').val();
        std.phone = $('#phoneno2').val();
        std.email = $('#emailname2').val();  
    }

    $.ajax({
        type: "POST",
        url: 'createFeedback',
        data: JSON.stringify(std),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {

            return true;

        },
        error: function () {

            return false;
        }
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function Unselect() {
    var ele = document.getElementsByName("rating1");
    for (var i = 0; i < ele.length; i++)
        ele[i].checked = false;


}

function Unselect2() {
    var ele2 = document.getElementsByClassName("check");
    for (var i = 0; i < ele2.length; i++)
        ele2[i].checked = false;
    removeHighlight2();

}

function onButtonClick() {
    document.getElementById('all').className = "open";

}

function onButtonClick1() {
    document.getElementById('all').className = "hide";

}

function onButtonClick2() {
    document.getElementById('test').className = "open";

}

function onButtonClick3() {
    document.getElementById('test').className = "hide";

}

function onButtonClick4() {
    document.getElementById('subject').className = "open";

}

function onButtonClick5() {
    document.getElementById('subject').className = "hide";

}

function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

function myFunction1() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "block") {
        x.style.display = "none";
    }
}

